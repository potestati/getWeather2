
var city_list = [];
var final_url_3 = '';
var new_city_template = '<div class="new-city col-sm-4"><div class="col-md-4 city-date"><h3 class="city-name">Lisbon</h3><p class="date">July 4</p></div><div class="col-md-3 temp-clouds"><h3 class="temperature">24</h3><p class="clouds">clouds</p></div><div class="col-md-5 hum-wind"><span>Humidity&#58; </span><p class="hum">hum</p><span>Wind&#58; </span><p class="wind">wind</p></div></div>';


function another_city(id) {
    city_list.push(id); // dodaje novi id u array
    console.log('...spisak ID gradova:');
    console.log(city_list);

    prikaz_rezultata(); // poziva konacni prikaz prognoze
}

function prikaz_rezultata() {
    // ispis informacija o gradovima
    console.log("___prikaz rezultata");
    var search_value_2 = ""; // ovo ce da sadrzi string sa vise id-va za vise gradova

    $(".weather").html(""); // isprazni prikaz prognoze za gradove

    // PROLAZI KROZ SVAKI GRAD
    $.each(city_list, function (index, value) {
        console.log("EACH+++++++++++++++");
        console.log(index + ": " + value);
        search_value_2 = search_value_2 + "," + value;
        console.log(search_value_2);

        var final_url_3 = 'http://api.openweathermap.org/data/2.5/weather?APPID=50500890cdcb59589cac496783decee1&units=metric&id=' + value;
        console.log(final_url_3);


        // AJAX NOVI ZA SVAKI GRAD SA JEDNIM ID-om
        $.get(final_url_3, function (data) {
            console.log("AJAX FINAL");
            console.log(data);

            $(".weather").append(new_city_template); // popunjava sa jednim praznim gradom

            console.log(data.name);
            $(".new-city .city-name").html(data.name);
            console.log(data.main.temp);
            // console.log( timestamp.format("HH/mm/ss/YYYY") );
            // var now_vreme = new Date();
            // console.log(now_vreme);
            
            /* Format Date */
            var formattedDate = new Date(data.dt); // source je data.dt
            console.log(formattedDate);
            var d = formattedDate.getDate();
            var m = formattedDate.getMonth();
            m += 1;  // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var final_dt = d + "." + m + "." + y
            var d = new Date();
            $(".new-city .date").html(d.toDateString());
           
            $(".new-city .temperature").html(data.main.temp);
            $(".new-city .clouds").html(data.clouds);
            $(".new-city .hum").html(data.main.humidity);
            $(".new-city .wind").html(data.wind.speed);

            $(".new-city").addClass("city").removeClass("new-city"); // uklanja privremenu klasu i stavlja finalnu

            console.log('Spinner hide...');
            // END OF AJAX ZA SVAKI GRAD
        });

        // END OF ITERATION
    });
}


function dodaj_grad(tag) {
    console.log('JEDAN GRAD TAG AJAX...');
    console.log('Spinner show...');
    var urlgradid = 'http://api.openweathermap.org/data/2.5/weather?APPID=50500890cdcb59589cac496783decee1&units=metric&q=' + encodeURIComponent(tag);

    // ajax search koji dobija ID samo tog grada iz taga
    $.get(urlgradid, function (data2) {
        console.log(tag);
        console.log('GRAD TAG sadryaj: ');
        console.log(data2);
        console.log(data2.id);
        another_city(data2.id);
    });
}
// tags-----

function onAddTag(tag) {
    // alert("Added a tag: " + tag);
    console.log("Added a tag: " + tag);
    dodaj_grad(tag);
}
function onRemoveTag(tag) {
    // alert("Removed a tag: " + tag);
    console.log("Removed a tag: " + tag);
}

function onChangeTag(input, tag) {
    // alert("Changed a tag: " + tag);
    console.log("Changed a tag: " + tag);
}

$(function () {
// document readz

//    $('#tags_1').tagsInput({width: 'auto'});

// Uncomment this line to see the callback functions in action
    $('input.tags').tagsInput({onAddTag: onAddTag, onRemoveTag: onRemoveTag, onChange: onChangeTag});

// Uncomment this line to see an input with no interface for adding new tags.
//			$('input.tags').tagsInput({interactive:false});
});

$(document).ready(function () {
    console.log('ready hello!!!');

    // STARI KOD ZA JEDAN GRAD
    $("#city-search").on('submit', function (e) {
        console.log('Spinner show...');

        var search_value = $("input.search-field").val();
        var openweatherapikey = '50500890cdcb59589cac496783decee1';
        var urlfinal = 'http://api.openweathermap.org/data/2.5/weather?APPID=50500890cdcb59589cac496783decee1&units=metric&q=' + encodeURIComponent(search_value);

        console.log(urlfinal);

        // ajax search
        $.get(urlfinal, function (data) {
            console.log(data);

            // smestanje podataka u druge promenjive...
            city_name = data.name;
            console.log(city_name);
            console.log(data.main);
            console.log(data.main.temp);


            // ovde ce da ide kod za dalju obradu podataka

            //var new_city_template ='<div class="new-city col-sm-4"><h3 class="city-name">Lisbon</h3><p class="date">July 4</p><h3 class="temperature">24</h3><p class="clouds">clouds</p><p class="hum">hum</p><p class="wind">wind</p></div>';
            var new_city_template = '<div class="new-city col-sm-4"><div class="col-md-4 city-date"><h3 class="city-name">Lisbon</h3><p class="date">July 4</p></div><div class="col-md-3 temp-clouds"><h3 class="temperature">24</h3><span>&deg;C</span><p class="clouds">clouds</p></div><div class="col-md-5 hum-wind"><p class="hum"><span>Humidity&#58; </span>hum</p><p class="wind"><span>Wind&#58; </span>wind</p></div></div>';
            // console.log(value);
            $(".weather").html(""); // isprazni element

            // ovde ine foreach ili .each
            $(".weather").append(new_city_template);
            console.log(data.name);
            $(".new-city .city-name").html(data.name);
            console.log(data.main.temp);

            // console.log( timestamp.format("HH/mm/ss/YYYY") );

            // var now_vreme = new Date();
            // console.log(now_vreme);

            $(".new-city .date").html(data.dt);
            $(".new-city .temperature").html(data.main.temp);
            console.log("data.clouds");
            console.log(data.clouds);
            $(".new-city .clouds").html(data.clouds);
            $(".new-city .hum").html(data.main.humidity);
            $(".new-city .wind").html(data.wind.speed);

            $(".new-city").addClass("city").removeClass("new-city");

            /*
             $.each( data, function( index, value ) {
             console.log(value);
             $(".weather").html(new_city_template);
             console.log(value.name);
             $(".new-city city-name").html(value.name);
             console.log(value.temp);
             $(".new-city temperature").html(value.temp);
             $(".new-city clouds").html(value.name);
             $(".new-city hum").html(value.name);
             $(".new-city wind").html(value.name);
             });
             * 
             */

            //

            console.log('foo bar');
            //neka_obrada_podataka(); // update main area
            //completed_processing();
            console.log('Spinner hide...');
        });
        // e.preventDefault();
        // e.stopPropagation();
    });

    // novi kod ya vise gradova je gore

});

